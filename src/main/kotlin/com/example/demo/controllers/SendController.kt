package com.example.demo.controllers

import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/*
   var name: String,
   var userName: String,
   var password: String,
   var currentState: CurrentState
*/
@RestController
@RequestMapping("/sendsubsdata")
class SendController {
  @Autowired
  private val speakerRepository: SpeakerRepository? = null

  @PostMapping
  fun create(@RequestBody speaker: Speaker?): Speaker {
    return speakerRepository.saveAndFlush(speaker)
  }

  @RequestMapping(value = ["{id}"], method = [RequestMethod.DELETE])
  fun delete(@PathVariable id: Long?) {
    //Also need to check for children records before deleting.
    speakerRepository.deleteById(id)
  }

  @RequestMapping(value = ["{id}"], method = [RequestMethod.PUT])
  fun update(@PathVariable id: Long?, @RequestBody speaker: Speaker?): Speaker {
    //because this is a PUT, we expect all attributes to be passed in. A PATCH would only need what has changed.
    //TODO: Add validation that all attributes are passed in, otherwise return a 400 bad payload
    val existingSpeaker: Speaker = speakerRepository.getOne(id)
    BeanUtils.copyProperties(speaker, existingSpeaker, "speaker_id")
    return speakerRepository.saveAndFlush(existingSpeaker)
  }
}
package com.example.demo.controllers

import com.example.demo.models.Subscriber
import org.apache.tomcat.jni.Lock.name
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/*
    var name: String,
    var userName: String,
    var password: String,
    var currentState: CurrentState
*/
@RestController
@RequestMapping("/getsubsdata")
class RetrieveController {

  lateinit var allSubsList: List<Subscriber>

  @GetMapping
  fun allSubsData() : List<Subscriber> = allSubsList

  @RequestMapping("/{name}")
  fun get(@PathVariable name: String?): Subscriber = allSubsList.find { allSubsList.indexOf(1).equals(name) }


}